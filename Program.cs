﻿using System;

namespace exercise_14
{
    class Program
    {
        static void Main(string[] args)
        {
              Console.WriteLine("Hi Karan ! Please type in the month you were born in.");
            var bornmonth = Console.ReadLine();

            Console.WriteLine($"The month I was born in is {bornmonth}");
        }
    }
}
